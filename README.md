# Template
Quickly start to build a responsive website.


#  Features
* Made for Responsive Design
* HTML and CSS validated by the W3C Validation Service without errors
* Clean and commented code
* jQuery ready
* Content max-width fixed to 1200px
* CSS with vendor prefixes validated by [Autoprefixer](https://github.com/postcss/autoprefixer)
* Safe margins for content placed inside `<div class=“content”>` from the iPhone notch
* Smooth anchor scroll powered by CSS


# How-to
* Add your code inside the `<div class=“content”>`
* Easily change the `font-family`  at the beginning of the CSS file


# Author
Hi! I’m [Julien Widmer](https://www.julienwidmer.ca) - Feel free to send me an [Email](mailto:hello@julienwidmer.ca) or to follow me on [Twitter](http://twitter.com/Qasph).